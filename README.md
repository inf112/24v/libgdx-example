# INF112 Example project


## `inf112.variant1` – simple application using libGDX `Actor`
* `inf112.variant1.app.ActorDemo` – run this to see the Actor demo
* `inf112.variant1.app.ActorApp` – app setup and render loop
* `inf112.variant1.app.Gator` – an example actor


## `inf112.variant2` – simple application using MVC
* `inf112.variant2.app.MVCDemo` – run this to see the MVC demo
* `inf112.variant2.app.Controller` – controller (and setup code)
* `inf112.variant2.model.ModelObject` – all objects in the model implement this
* `inf112.variant2.model.ModelFactory` – instantiated concrete model objects based on name
* `inf112.variant2.view.View` – view code

## `inf112.flocking` – flocking behaviour demonstration
(Similar to `inf112.variant1` apart from the flocking.)
* `inf112.variant1.app.FlockingDemo` – run this to see the Flocing demo
* `inf112.variant1.app.FlockingApp` – app setup and render loop
* `inf112.variant1.app.Bird` – actor with flocking behaviour

## `inf112.util` – helper classes
* `inf112.util.Calculations` – helper for calculating collision with the screen edge
* `inf112.util.PluginLoader` – dynamically finds and loads classes (see example in `inf112.variant2.model.ModelFactory`); doesn't work when packaged in a JAR file





# Credits

### Template example files
* `src/main/resources/obligator.png` – Ingrid Næss Johansen
* `src/main/resources/pipp.png` – Anya Helene Bagge
* `src/main/resources/blipp.ogg`– Dr. Richard Boulanger et al (CC-BY-3.0)
* `src/main/resources/apple*.png` – OtuNwachinemere, [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0), via Wikimedia Commons
