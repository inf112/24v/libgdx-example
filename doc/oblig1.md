# Rapport – innlevering 1
**Team:** *Teamnavn* – *medlemmer*...

## Konsept

Spillet handler om en geit som er ute på beite i en fjellside/bakke (2D sideveis (eller diagonal) scrolling). Målet er å spise mest mulig mat på veien, komme seg forbi hindringer og lengst mulig opp på fjellet før det blir mørkt.

Tanken bak spillet er å illustrere en del konsepter innen software engineering, samt gi et lite innblikk i det økologiske konseptet [endozookori](https://en.wikipedia.org/wiki/Seed_dispersal#By_animals).

## MVP

* 2D-skjerm med bakke som scroller sideveis eller diagonalt
* Enkel blå himmel bak som blir mørkere etterhvert som dagen går
* En geit som kan kontrolleres av spilleren
* Spilleren kan gå og hoppe over hindringer
* Spisbare planter plassert på bakken
* Enkle hindringer
* En timer som avgjør spillslutt

## Videre mål

* Mer avanserte hindringer (bro med troll, f.eks.?)
* Mat som man bare kan nå ved å hoppe opp
* Akrobatiske bevegelser
* Alternative veier oppover, f.eks. vanskelig klatring opp en bratt fjellside
* Flere levels (dager)
* Enkel simulering av frøspredning hvor plantene begynner å vokse lenger oppe

## Stretch goals

* Mer avansert mål for spilleren; f.eks. «flytte» spesifikker arter til spesifikke soner
* Andre dyr, etc. man kan interagere med
* Customisering av utseende/evner til geiten
* Flere spiller, f.eks. med shared eller split screen

# Krav

## Ikke-funksjonelle krav

Bruke MVC, dependency injection. Sørg for at spill-logikken kan kjøre uten å linke med libGDX/grafikk. Gode tester.

## Funksjonelle krav

… Utdyp punktene fra konseptbeskrivelsen, med brukerhistorier, akseptansekriterier

…


