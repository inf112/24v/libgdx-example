package inf112.eksamen24;

public  class AnimalBuilder {
	String name;
	int x,y;
	double weight;
	/*
	 * Hvis vi vil bygge flere typer objekter:
	 * 
	 * * En metode for createCat (gir CatBUilder), createDog() …
	 * * "Oppgradere" til mer spesifikk type underveis
	 * * En "sub-builder" for spesfikke opsjoner
	 * 
	 * Poenget er å gjøre bruken av API-et lettere:
	 * 
	 * * Unngå lange parameterlister for konstruktøren
	 * * Lettere å ha noen valgfrie valg
	 * * Ha en sjekk på at feltene er lovlige/konsistente
	 * * Lett å gjøre objektet immutable etter at det er laget
	 * * Lett for programmøren å utforske i IDE 
	 * 
	 * IDE = Integrert Utviklingsmiljø, Integrated Development Environment
	 * 
	 * VSCode, Emacs, Vim, SublimeText: for å editere tekst
	 * 
	 * IDE: Eclipse, IntelliJ, Visual Studio, XCode, 
	 * 
	 */
	static {
		CatBuilder catBuilder = AnimalBuilder.catBuilder();
		catBuilder.name("Mons").fur("Tabby").build();
		AnimalBuilder.create().name("Mons").cat().fur("…");
	}
	public static CatBuilder catBuilder() {
		return new CatBuilder();
	}
	AnimalBuilder name(String name) {
		this.name = name;
		if(name .equals("Lillemor")) {
			return new CatBuilder(this);
		}
		return this;
	}
	/*AnimalBuilder setName(String name) {
		this.name = name;
		return this;
	}*/
	
	CatBuilder fur() {
		return new CatBuilder(this);
	}
	CatBuilder cat() {
		return new CatBuilder(this);
	}
	
	 Animal build() {
		 throw new UnsupportedOperationException();
	 }
	
	 NameTagBuilder addNameTag() {
		 
	 }
	public static AnimalBuilder create() {
		return new AnimalBuilder();
	}
	static {
		/*
		IValueFactory vf = null;
		
		vf.integer(23); // ← Factory
		vf.list(3, 4, 5, 6, 7);
		vf.listWriter() // ← Builder
			.add(3).add(4).add(5).add(6).build();
			*/
		
	}
	
	
	
}

