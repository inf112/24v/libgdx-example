package inf112.eksamen24;

public interface Animal {

	String name();
	int x();
	int y();
	double weight();
	String fur();
}
