package inf112.flocking;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.graphics.Texture;

public class FlockingApp implements ApplicationListener {
	public static Bird bird;
	public static Vector2 target = new Vector2();

	private Texture birdTex;
	private Sound bellSound;
	private Stage stage;
	private Random random = new Random();
	private List<Bird> birds = new ArrayList<>();

	@Override
	public void create() {
		// Called at startup

		stage = new Stage(new ScalingViewport(Scaling.fit, 3840, 2400));
		birdTex = new Texture(Gdx.files.internal("pipp.png"));
		bellSound = Gdx.audio.newSound(Gdx.files.internal("blipp.ogg"));

		var listener = new InputAdapter() {
			@Override
			public boolean keyDown(int keyCode) {
				if (keyCode == Keys.LEFT || keyCode == Keys.A) {
					System.out.println("left");
					bird.setVelocity(-100, 0);
					return true;
				} else if (keyCode == Keys.RIGHT || keyCode == Keys.D) {
					System.out.println("right");
					bird.setVelocity(100, 0);
					return true;
				}
				return false;

			}
		};
		var multiplexer = new InputMultiplexer(listener, stage);
		Gdx.input.setInputProcessor(multiplexer);

		Gdx.graphics.setForegroundFPS(60);
	}

	@Override
	public void dispose() {
		// Called at shutdown

		// Graphics and sound resources aren't managed by Java's garbage collector, so
		// they must generally be disposed of manually when no longer needed. But,
		// any remaining resources are typically cleaned up automatically when the
		// application exits, so these aren't strictly necessary here.
		// (We might need to do something like this when loading a new game level in
		// a large game, for instance, or if the user switches to another application
		// temporarily (e.g., incoming phone call on a phone, or something).

		birdTex.dispose();
		bellSound.dispose();
	}

	@Override
	public void render() {
		// Called when the application should draw a new frame (many times per second).

		// This is a minimal example – don't write your application this way!

		// Start with a blank screen
		ScreenUtils.clear(Color.BLACK);

		if (birds.size() < 500) {
			bird = new Bird(birdTex, 50,50);
			bird.setVelocity(50 * (random.nextFloat() - .5f), 50 * (random.nextFloat() - .5f));
			bird.setPosition((.25f+random.nextFloat()/2) * stage.getWidth(), (.25f+random.nextFloat()/2) * stage.getHeight());
			stage.addActor(bird);
			birds.add(bird);
		}
		if (Gdx.input.isTouched()) {
			//System.out.println("bell!");
			target.set(Gdx.input.getX(), Gdx.input.getY());
			stage.screenToStageCoordinates(target);
			//bird.setPosition(target.x, target.y, Align.center);
			
			//bellSound.play();
		}
		
		float delta = Gdx.graphics.getDeltaTime();
		stage.act(delta);
		stage.draw();

		target.set(0,0);

		// bird.step();

		//System.out.println(stage.getWidth() + "x" + stage.getHeight());

		if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) { // check for key press
			Gdx.app.exit();
		}
	}

	@Override
	public void resize(int width, int height) {
		// Called whenever the window is resized (including with its original site at
		// startup)

		stage.getViewport().update(width, height, true);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
