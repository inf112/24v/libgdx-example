package inf112.flocking;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

public class FlockingDemo {
    public static void main(String[] args) {
        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setTitle("Flocking Demo");
        cfg.setWindowedMode(1920, 1080);

        new Lwjgl3Application(new FlockingApp(), cfg);
    }
}