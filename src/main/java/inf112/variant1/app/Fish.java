package inf112.variant1.app;

import com.badlogic.gdx.scenes.scene2d.Actor;

public class Fish extends Actor {
	public float health = 10f;
	public void kill() {
		health = 0;
	}
	public float getHealth() {
		return health;
	}
}
