package inf112.variant1.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class ActorApp implements ApplicationListener {
	private Sound bellSound;
	public static Gator gator;
	private Stage stage;
	private List<Gator> gators = new ArrayList<>();
	private Texture gatorTex, pippTex;
	public static Vector2 target = new Vector2();
	private Random random = new Random();

	@Override
	public void create() {
		// Called at startup

		stage = new Stage(new ScalingViewport(Scaling.fit, 1920, 1080));
		gatorTex = new Texture(Gdx.files.internal("obligator.png"));
		pippTex = new Texture(Gdx.files.internal("pipp.png"));

		bellSound = Gdx.audio.newSound(Gdx.files.internal("blipp.ogg"));

		var listener = new InputAdapter() {
			@Override
			public boolean keyDown(int keyCode) {
				if (keyCode == Keys.LEFT || keyCode == Keys.A) {
					System.out.println("left");
					gator.setDirection(-100, 0);
					return true;
				} else if (keyCode == Keys.RIGHT || keyCode == Keys.D) {
					System.out.println("right");
					gator.setDirection(100, 0);
					return true;
				}
				return false;

			}
		};
		var multiplexer = new InputMultiplexer(listener, stage);
		Gdx.input.setInputProcessor(multiplexer);

		Gdx.graphics.setForegroundFPS(60);
	}

	@Override
	public void dispose() {
		// Called at shutdown

		// Graphics and sound resources aren't managed by Java's garbage collector, so
		// they must generally be disposed of manually when no longer needed. But,
		// any remaining resources are typically cleaned up automatically when the
		// application exits, so these aren't strictly necessary here.
		// (We might need to do something like this when loading a new game level in
		// a large game, for instance, or if the user switches to another application
		// temporarily (e.g., incoming phone call on a phone, or something).
		gatorTex.dispose();
		bellSound.dispose();
	}


	@Override
	public void render() {
		// Called when the application should draw a new frame (many times per second).

		// Start with a blank screen
		ScreenUtils.clear(Color.BLACK);

		if (gators.size() < 200) {
			var tex = random.nextBoolean() ? gatorTex : pippTex;
			var pos = randomPosition();
			gator = new Gator(tex, 100, tex.getHeight() * 100/tex.getWidth());
			gator.setVelocity(randomSpeed(), randomSpeed());
			gator.setPosition(pos.x, pos.y);
			stage.addActor(gator);
			gators.add(gator);
		}
		
		if (Gdx.input.isTouched()) {
			//System.out.println("bell!");
			target.set(Gdx.input.getX(), Gdx.input.getY());
			stage.screenToStageCoordinates(target);
			//gator.setPosition(target.x, target.y, Align.center);
			
			//bellSound.play();
		}
		
		float delta = Gdx.graphics.getDeltaTime();
		stage.act(delta);
		stage.draw();

		target.set(0,0);

		if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) { // check for key press
			Gdx.app.exit();
		}
	}
	private Vector2 randomPosition() {
		return new Vector2((.25f+random.nextFloat()/2) * stage.getWidth(), (.25f+random.nextFloat()/2) * stage.getHeight());
	}
	private float randomSpeed() {
		return 500 * (random.nextFloat() - .5f); // random.nextGaussian()*100
	}
	@Override
	public void resize(int width, int height) {
		// Called whenever the window is resized (including with its original site at
		// startup)

		stage.getViewport().update(width, height, true);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
