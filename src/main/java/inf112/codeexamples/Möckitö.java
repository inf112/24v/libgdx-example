package inf112.codeexamples;

import java.lang.invoke.MethodHandle;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

// “We have Mockito at home!”
//
// Mockito at home:
//
public class Möckitö {
	//////////////////////////////////////////////
	//
	// Context keeps track of implementation and which calls have been made
	//
	//
	// keep track of method calls
	static final Set<CallSignature> calls = new HashSet<>();

	// store the answers for particular method calls
	static final Map<CallSignature, Object> answers = new HashMap<>();

	// last method call (for when())
	static CallSignature lastMethodCalled = null;

	// for the proxy-based implementation
	static final Map<Class<?>, InvocationHandler> proxyHandlers = new IdentityHashMap<>();

	/**
	 * Can't make instance of this class
	 */
	private Möckitö() {

	}

	/**
	 * Create a mock object that implements an interface
	 * 
	 * Only works with interfaces, not classes.
	 * 
	 * Example:
	 * 
	 * <pre>
	 * List mockList = mock(List.class);
	 * mockList.add("banana"); // call any method
	 * assert mockList.get(0) == null; // return is null, 0 or false by default
	 * </pre>
	 * 
	 * @param <T>       The interface type
	 * @param realClass The class object of an interface.
	 * @return An mock object of type T
	 * @see org.mockito.Mockito#mock
	 */
	public static <T> T mock(Class<T> realClass) {
		// We can't really implement it this way; we need reflection to do this
		// (see below), otherwise we'd have to manually implement mocked versions
		// of all classes.
		if (realClass == List.class)
			return (T) new MockList(realClass.getName());

		// A very simple reflective implementation – this only works
		// if realClass is an interface. Mockito uses the same technique when it can,
		// otherwise it'll generate code for a new class.
		if (Modifier.isInterface(realClass.getModifiers())) {
			InvocationHandler handler = proxyHandlers.get(realClass);
			if (handler == null) {
				handler = new DynamicMockHandler(realClass.getName());
				proxyHandlers.put(realClass, handler);
			}
			Class<?>[] interfaces = new Class[] { realClass, MockObject.class };
			// A proxy object will be marked as implementing the given interface;
			// whenever a method is called, it'll call our handler with
			// the object, method name and method arguments as argument
			return (T) Proxy.newProxyInstance(Möckitö.class.getClassLoader(), //
					interfaces, handler);
		}
		throw new UnsupportedOperationException("Can only mock interfaces");
	}

	/**
	 * Check if a method has been called on a mock object.
	 * 
	 * Raises an assertion error if the method hasn't been called or if the argument
	 * were different.
	 * 
	 * For example:
	 * 
	 * <pre>
	 * List mockList = mock(List.class);
	 * mockList.add("kiwi");
	 * verify(mockList).add("kiwi"); // OK
	 * verify(mockList).add("pear"); // AssertionError (different argument)
	 * verify(mockList).get(0); // AssertionError (get hasn't been called)
	 * </pre>
	 * 
	 * @param <T>  Type of the mock object
	 * @param mock A mock object
	 * @return The same mock object – call a method on it to verify that the method
	 *         has been called
	 * @see org.mockito.Mockito#verify
	 */
	public static <T> T verify(T mock) {
		((MockObject) mock)._startVerify();
		return mock;
	}

	/**
	 * Specify method behaviour.
	 * 
	 * For example:
	 * 
	 * <pre>
	 * List mockList = mock(List.class);
	 * when(mockList.get(0)).thenReturn("kiwi");
	 * </pre>
	 * 
	 * @param <T> Method return type
	 * @param t   Result of a call to one of the mock object's methods
	 * @return An object with a {@link OngoingStubbing#thenReturn(Object)} method
	 * @see org.mockito.Mockito#when
	 */
	public static <T> OngoingStubbing<T> when(T t) {
		return new OngoingStubbing<T>() {
			@Override
			public OngoingStubbing<T> thenReturn(T value) {
				if (lastMethodCalled != null) {
					answers.put(lastMethodCalled, value);
					lastMethodCalled = null;
					return this;
				} else {
					throw new IllegalStateException();
				}
			}
		};
	}

	/**
	 * Used by {@link #verify(Object)}.
	 * 
	 * Throws <code>new AssertionError(message)</code> if <code>!b</code>.
	 * 
	 * @param b       A value to test
	 * @param message Message for the exception
	 */
	public static void assertTrue(boolean b, String message) {
		if (!b) {
			throw new AssertionError(message);
		}
	}

	/**
	 * Used to specify the behaviour of a stub method.
	 * 
	 * (Supports only <code>thenReturn()</code>.
	 * 
	 * @param <T>
	 * @see org.mockito.stubbing.OngoingStubbing
	 */
	public interface OngoingStubbing<T> {
		OngoingStubbing<T> thenReturn(T value);
	}

	/**
	 * All mock objects will implement this interface.
	 * 
	 * @author anya
	 *
	 */
	interface MockObject {
		/**
		 * Called by Möckitö when {@link Möckitö#verify(Object)} is called on the mock
		 * object.
		 */
		void _startVerify();
	}

	/**
	 * Used to store a particular method call.
	 * 
	 * @author anya
	 *
	 */
	static record CallSignature(/**
								 * Unique id for the mock object
								 */
	int objectId, /**
					 * Name of the method
					 */
	String methodName, /**
						 * The argument list
						 */
	Object... args) {

		@Override
		public String toString() {
			return "CallSignature [objectId=" + objectId + ", methodName=" + methodName + ", args="
					+ Arrays.toString(args) + "]";
		}

		// Needed since we'll use the signature as key in a hash table
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Arrays.deepHashCode(args);
			result = prime * result + Objects.hash(methodName, objectId);
			return result;
		}

		// Needed since we'll use the signature as key in a hash table
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			CallSignature other = (CallSignature) obj;
			return Arrays.deepEquals(args, other.args) && Objects.equals(methodName, other.methodName)
					&& objectId == other.objectId;
		}

	}

	/**
	 * Implementation of MockObject for the List interface.
	 *
	 * (Only implements get(), size(), add(), contain(), addAll().)
	 * 
	 * The real Mockito will likely generate code somewhat similar to this when
	 * you're mocking a class.
	 * 
	 * @param <T> List element type
	 */
	static class MockList<T> extends AbstractList<T> implements List<T>, MockObject {
		// name of the mocked interface (for toString())
		private String className;
		// true if we just saw a call to verify()
		boolean verifyActive = false;

		MockList(String className) {
			this.className = className;
		}

		public void _startVerify() {
			verifyActive = true;
		}

		/**
		 * Implementation of a mock method
		 * 
		 * @param <U>        The return type
		 * @param methodName Method name
		 * @param dflt       Default return value
		 * @param args       List of arguments to the method call
		 * @return <code>dflt</code> or a value previously set by
		 *         {@link Möckitö#when(Object)}
		 */
		<U> U stubMethodImpl(String methodName, U dflt, Object... args) {
			var objId = System.identityHashCode(this);
			var sig = new CallSignature(objId, methodName, args);

			if (verifyActive) {
				assertTrue(calls.contains(sig), //
						"Expected call to " + className + "::" + methodName //
								+ " with arguments " + Arrays.toString(args));

				verifyActive = false;
			} else {
				calls.add(sig);
				lastMethodCalled = sig;
				System.err.println("MockList: Registered call to " + sig);
			}
			if (answers.containsKey(sig))
				return (U) answers.get(sig);
			else
				return dflt;
		}

		@Override
		public String toString() {
			return "mock(" + className + ")@" + System.identityHashCode(this);
		}

		// Implement specific methods for List

		@Override
		public T get(int i) {
			return stubMethodImpl("get", null, i);
		}

		@Override
		public int size() {
			return stubMethodImpl("size", 0);
		}

		@Override
		public boolean add(T e) {
			return stubMethodImpl("add", false, e);
		}

		@Override
		public boolean contains(Object e) {
			return stubMethodImpl("contains", false, e);
		}

		@Override
		public boolean addAll(Collection<? extends T> c) {
			return stubMethodImpl("addAll", false, c);
		}

		// we have "cheated" and used AbstractList to provide the
		// other methods since there are a lot of them – but
		// they could all be easily (and boringly) implemented
		// with stubMethodImpl()
		// (Inheriting from AbstactList won't really work for
		// making an actual mock object, since we're only
		// intercepting a few of the methods.)

		// … insert more methods here …
	}

	/**
	 * “Fancy” implementation which should work for any interface.
	 * 
	 * This doesn't actually implement {@link MockObject}, it's just a handler for a
	 * <em>proxy object</em>. Any method call on the proxy object will be forwarded
	 * to this handler, which provides the actual functionality of the mock object.
	 * 
	 * We need one handler for each interface we're mocking, but objects mocking the
	 * same interface can share handlers.
	 * 
	 * Real Mockito uses the same technique when you're mocking an interface.
	 * 
	 * @author anya
	 * @see java.lang.reflect.Proxy
	 */
	static class DynamicMockHandler implements InvocationHandler {

		// name of the mocked interface (for toString())
		private String className;
		// true if we just saw a call to verify()
		private boolean verifyActive = false;

		DynamicMockHandler(String className) {
			this.className = className;
		}

		// This method gets called for every call to the mock
		// object's method
		@Override
		public Object invoke(Object proxy, Method method, Object[] args) {
			var nArgs = args != null ? args.length : 0;
			// implement MockObject interface
			if (method.getName().equals("_startVerify") && nArgs == 0) {
				verifyActive = true;
				return null;
			}
			
			var objId = System.identityHashCode(proxy);
			var methodId = method.toGenericString();
			var sig = new CallSignature(objId, methodId, args);
			
			System.err.println("invoke: " + methodId + "(" + Arrays.toString(args) + ")");

			if (verifyActive) {
				assertTrue(calls.contains(sig), //
						"Expected call to " + className + "::" + method.getName() //
								+ " with arguments " + Arrays.toString(args));
				verifyActive = false;
			} else {
				calls.add(sig); // for later calls to verify()
				lastMethodCalled = sig; // for impl of when()
				System.err.println("Proxy: Registered call to " + sig);
			}

			if (answers.containsKey(sig))
				return answers.get(sig);
			// default implementation of toString()
			// (we should probably also prove equals(), hashCode() etc)
			else if (method.getName().equals("toString") && nArgs == 0)
				return "mock(" + className + ")@" + objId;
			// default return values for primitive types
			else if (method.getReturnType() == Float.TYPE)
				return Float.valueOf(0);
			else if (method.getReturnType() == Double.TYPE)
				return Double.valueOf(0);
			else if (method.getReturnType() == Integer.TYPE)
				return Integer.valueOf(0);
			else if (method.getReturnType() == Short.TYPE)
				return Short.valueOf((short) 0);
			else if (method.getReturnType() == Byte.TYPE)
				return Byte.valueOf((byte) 0);
			else if (method.getReturnType() == Boolean.TYPE)
				return Boolean.valueOf(false);
			else if (method.getReturnType() == Character.TYPE)
				return Character.valueOf('\0');
			return null;
		}
	}
}