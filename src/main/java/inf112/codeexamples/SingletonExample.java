package inf112.codeexamples;

public class SingletonExample {

	public static final SingletonExample instance = new SingletonExample();
	private String state = "default";
	
	public void setLineColor(String s) {
		state = s;
	}
	public String getLineColor() {
		return state;
	}
	private SingletonExample() {}
	
	public static SingletonExample getInstance() {
		return instance;
	}
	
	
}
