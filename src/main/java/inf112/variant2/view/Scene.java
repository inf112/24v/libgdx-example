package inf112.variant2.view;

public interface Scene {
	Iterable<String> requiredTextures();
}
