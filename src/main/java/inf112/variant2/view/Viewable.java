package inf112.variant2.view;

/**
 * Interface for objects that can be drawn by View.
 * 
 * @author anya
 *
 */
public interface Viewable {
	float x();

	float y();

	float angle();

	float width();

	float height();

	float scale();

	String skin();

}
