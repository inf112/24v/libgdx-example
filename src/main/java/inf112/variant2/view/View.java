package inf112.variant2.view;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.Viewport;

public class View {
	private Map<String, Texture> textures = new HashMap<>();
	private SpriteBatch batch;
	private Texture emptyTexture;
	private Viewport viewport;
	private BitmapFont font;
	private boolean debugCoords = false;

	public View(Viewport viewport) {
		this.font = new BitmapFont(Gdx.files.internal("fonts/garamond.fnt"));
		this.batch = new SpriteBatch();
		this.emptyTexture = new Texture(1, 1, Format.RGBA8888);
		this.viewport = viewport;
	}

	public void loadTextures(Scene s) {
		for (var tex : textures.values()) {
			tex.dispose();
		}
		for (var texName : s.requiredTextures()) {
			loadTexture(texName + ".png");
		}
	}

	public Texture loadTexture(String texName) {
		Texture tex = emptyTexture;
		try {
			tex = new Texture(Gdx.files.internal(texName));
			Gdx.app.log("View", "loaded " + texName);
		} catch (GdxRuntimeException e) {
			Gdx.app.error("View", "File not found: " + texName);
		}
		textures.put(texName, tex);
		return tex;
	}

	public void drawFrame(List<? extends Viewable> objects) {
		// Start with a blank screen
		ScreenUtils.clear(Color.WHITE);
		viewport.apply(true);
		batch.begin();

		for (var o : objects) {
			drawObject(o);
		}
		font.setColor(Color.BLACK);
		if (debugCoords) {
			int w = (int) viewport.getWorldWidth(), h = (int) viewport.getWorldHeight();
			var v = new Vector2(1920, 1080);
			font.draw(batch, "0,0", 0, 50, 150, Align.left, true);
			font.draw(batch, w + ",0", v.x - 150, 50, 150, Align.right, true);
			font.draw(batch, "0," + h, 0, v.y, 150, Align.left, true);
			font.draw(batch, w + "," + h, v.x - 250, v.y, 250, Align.right, true);
		}
		batch.end();
	}

	private void drawObject(Viewable viewObj) {
		var skin = viewObj.skin();

		var texture = textures.get(skin);
		if (texture == null)
			texture = loadTexture(skin);
		if (texture != emptyTexture) {
			float s = viewObj.scale();
			float w = viewObj.width() * s, h = viewObj.height() * s;
			float x = viewObj.x() - w / 2, y = viewObj.y() - h / 2;
			int tw = texture.getWidth(), th = texture.getHeight();
			float r = viewObj.angle();
			if (h == -1)
				h = (w * th) / tw;
			
			//batch.draw().at(x, y).origin(w/2, h/2)…;
			
			batch.draw(texture, x, y, // position
					w / 2, h / 2, // origin
					w, h, // size
					1, 1, // scale
					r, // rotation
					0, 0, // texture offset
					tw, th, // texture size
					false, r > 90 && r < 270); // flip
		}
	}
}
