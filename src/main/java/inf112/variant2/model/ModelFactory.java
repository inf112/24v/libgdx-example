package inf112.variant2.model;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Json;
import inf112.util.PluginLoader;

/**
 * This factory produces <code>ModelObject</code>s.
 * 
 * @author anya
 *
 */
public interface ModelFactory {

	static Map<String, Supplier<ModelObject>> factories = new HashMap<>();

	public static ModelObject verySimpleCreate(String name) {
		switch (name) {
		case "pipp":
			return new PippImpl();
		case "apple":
			return new AppleImpl();
		default:
			throw new IllegalArgumentException("Dont know how to make " + name);
		}

	}

	// This is a straight-forward way to set up the factory, though we should
	// maybe put this code together with the application startup.
	public static void simpleInitialize() {
		register("pipp", () -> new PippImpl());
		register("obligator", () -> new GatorImpl());
	}

	// Fancy initialization – will automatically load and register all classes
	// that implement the ModelObject interface.
	public static void autoInitialize() {
		// ModelFactory.class.getResourceAsStream("/birds/pipp.png");
		PluginLoader.loadClasses(ModelFactory.class, "..", ModelObject.class).forEach(c -> {
			System.out.println(c.getName());
			String name = PluginLoader.getConstant(c, "NAME", String.class);
			if (name == null)
				name = c.getSimpleName().replaceAll("Impl$", "");
			Supplier<ModelObject> factory = PluginLoader.makeFactory(c);
			if (factory != null) {
				factories.put(name, factory);
				Gdx.app.log("ModelFactory", String.format("Loaded ‘%s’ (from %s)", name, c.getName()));
			}
		});
		;
	}

	public static void initializeFromJson() {
		var json = new Json();
		@SuppressWarnings("unchecked")
		List<ObjectData> objs = json.fromJson(List.class, ObjectData.class, Gdx.files.internal("objects.json"));
		for (ObjectData obj : objs) {
			if (obj.kind == null || obj.implementBy == null || obj.textures == null) {
				Gdx.app.log("ModelFactory", "Object kind, implementBy or textures missing: " + json.toJson(obj));
			}

			List<Behaviour> behaviours = obj.behaviours.stream().map(ModelFactory::findBehaviour).toList();
			Supplier<AbstractModelImpl> cons = ModelFactory.findConstructor(obj.implementBy, obj.kind, obj.textures,
					behaviours);
			factories.put(obj.kind, () -> {
				ModelObject impl = cons.get();
				impl.size(obj.baseWidth, obj.baseHeight);
				return impl;
			});
		}
	}

	public static Behaviour findBehaviour(String name) {
		Class<Behaviour> clazz;
		if (name.contains("."))
			clazz = PluginLoader.loadClass(name, Behaviour.class);
		else
			clazz = PluginLoader.loadClass("inf112.variant2.model.behaviour." + name, Behaviour.class);
		try {
			return clazz.getDeclaredConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			Gdx.app.log("ModelFactory", "failed to instantiate " + name, e);
			return null;
		}
	}

	public static Supplier<AbstractModelImpl> findConstructor(String implementBy, String kindName,
			List<String> textures, List<Behaviour> behaviours) {

		switch (implementBy) {
		case "DefaultImpl":
			return () -> new DefaultImpl(kindName, textures, behaviours);
		case "PippImpl":
			return () -> new PippImpl();
		case "GatorImpl":
			return () -> new GatorImpl(kindName, textures, behaviours);
		default:
			throw new IllegalArgumentException("Unknown implementation " + implementBy);
		}
	}

	public static ModelObject create(String kind) {
		Supplier<ModelObject> modelFactory = factories.get(kind);
		if (modelFactory != null) {
			return modelFactory.get();
		}
		throw new RuntimeException("Don't know how to make " + kind);
	}

	public static List<String> kinds() {
		return List.copyOf(factories.keySet());
	}

	public static void register(String name, ModelFactory fact) {
		factories.put(name, () -> fact.create());
	}

	public ModelObject create();

	interface ModelConstructor {
		AbstractModelImpl construct(String kindName);
	}
}
