package inf112.variant2.model;

import java.util.List;
import java.util.function.Predicate;

import inf112.variant2.model.behaviour.DoFirstOfStrategy;
import inf112.variant2.model.behaviour.EatFoodBehaviour;
import inf112.variant2.model.behaviour.FlockingBehaviour;
import inf112.variant2.model.behaviour.GotoFoodBehaviour;
import inf112.variant2.model.behaviour.IfThenElse;
import inf112.variant2.model.behaviour.MoveBehaviour;
import inf112.variant2.model.behaviour.BounceBehaviour;

class PippImpl extends AbstractModelImpl {
	public static final String NAME = "pipp";
	ModelObject target;
	Behaviour currentBehaviour;
	boolean hungry = false;
	float time = 0;
	private boolean eating;


	public PippImpl() {
		super(NAME,  List.of("pipp.png"), List.of());
		this.behaviours.clear();
		// we use this to check if something is food
		// might be sensible to make this part of the ModelObject API
		Predicate<ModelObject> isFood = (x) -> {
			if (x.name().equals("apple")) {
				target = x;
				return true;
			} else {
				return false;
			}
		};
		// check if we're hungry
		Behaviour isHungry = (obj, dt, world) -> hungry;

		Behaviour eatFood = new IfThenElse(isHungry, new EatFoodBehaviour(isFood), null);
		Behaviour gotoFood = new IfThenElse(isHungry, new GotoFoodBehaviour(isFood), null);
		Behaviour flocking = new FlockingBehaviour();
		this.behaviours.add(new DoFirstOfStrategy(//
				eatFood, // if hungry, try to eat food ...
				gotoFood, // or find food
				flocking, // otherwise fall back to flocking
				new BounceBehaviour())); // fallback for the fallback
		// always "move" – the other behaviours will may change
		// direction, but won't actually move the object
		// this.behaviours.add(new BounceBehaviour());
		this.behaviours.add(new MoveBehaviour());
	}

	/**
	 * Override angle to "wiggle" the sprite when we're eating.
	 * 
	 * (The view will rotate according to angle().)
	 */
	@Override
	public float angle() {
		float angle = super.angle();
		if (eating) {
			float wiggle = (float) Math.sin(20 * time);
			return angle + 15 * wiggle;
		} else {
			return angle;
		}
	}

	@Override
	public float scale() {
		// don't disappear entirely if we're low on energy
		return .5f + (energy / maxEnergy) / 2f;
	}

	@Override
	public ModelObject act(float deltaTime, World world) {
		time += deltaTime;
		eating = false;
		// most of the work is done by the behaviour system
		super.act(deltaTime, world);

		// set a flag if we're hungry – we should probably
		// refactor this so it's common to all objects that "eat"
		if (!hungry && energy < maxEnergy * .5f) {
			hungry = true;
			// System.out.printf("hungry (%.2f/%.2f!)!%n", energy, maxEnergy);
		} else if (hungry && energy > maxEnergy * .9f) {
			hungry = false;
			// System.out.printf("not hungry (%.2f/%.2f!)!%n", energy, maxEnergy);
		}

		return this;

	}

	@Override
	public boolean eat(ModelObject other, float deltaTime, World world) {
		// set a flag if we're currently eating – we should probably
		// refactor this so it's common to all objects that "eat"
		// (See #angle())
		eating = super.eat(other, deltaTime, world);
		return eating;
	}
}
