package inf112.variant2.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.math.Vector2;

import inf112.variant2.model.behaviour.BounceBehaviour;
import inf112.variant2.view.Viewable;

abstract class AbstractModelImpl implements Viewable, ModelObject {
	protected float maxBite = 10f;
	protected float maxEnergy = 100 * maxBite;
	protected Vector2 position = new Vector2();
	// we switched from using a single velocity vector, to
	// using direction + speed – this is more convenient when
	// we do a lot of "turn towards food" behaviour, and it lets
	// us keep track of direction even if we're stopped
	protected Vector2 direction = new Vector2();
	protected float speed = 100f;
	protected Vector2 size = new Vector2();
	protected String name;
	protected transient List<Behaviour> behaviours;
	protected float energy;
	protected List<String> textures;

	public AbstractModelImpl(String name) {
		this(name, List.of(name + ".png"), List.of());
	}

	public AbstractModelImpl(String kindName, List<String> textures, List<Behaviour> behaviours) {
		this.maxBite = maxEnergy / 10f;
		this.energy = maxEnergy;
		this.name = kindName;
		this.textures = new ArrayList<>(textures);
		this.behaviours = new ArrayList<>(behaviours);
		if (behaviours.isEmpty())
			this.behaviours.add(new BounceBehaviour());
		if(textures.isEmpty())
			this.textures.add(kindName + ".png");
	}

	@Override
	public ModelObject act(float deltaTime, World world) {

		for (Behaviour b : behaviours) {
			b.act(this, deltaTime, world);
		}

		return this;
	}

	@Override
	public String skin() {
		return textures.get(0);
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public float x() {
		return position.x;
	}

	@Override
	public float y() {
		return position.y;
	}

	@Override
	public Vector2 direction() {
		return new Vector2(direction);
	}

	@Override
	public ModelObject turnTo(Vector2 newDirection) {
		if (newDirection.x != 0 || newDirection.y != 0)
			direction.set(newDirection).nor();
		return this;
	}

	@Override
	public float angle() {
		return direction.angleDeg();
	}

	@Override
	public String state() {
		return "standing"; // TODO
	}

	@Override
	public ModelObject accelerate(double ddx, double ddy) {
		Vector2 velocity = velocity();
		return accelerateTo(velocity.x + ddx, velocity.y + ddy);
	}

	@Override
	public ModelObject accelerate(Vector2 ddxy) {
		Vector2 velocity = velocity();
		return accelerateTo(velocity.x + ddxy.x, velocity.y + ddxy.y);
	}

	@Override
	public ModelObject accelerateTo(double dx, double dy) {
		if (dx != 0 || dy != 0) {
			direction.set((float) dx, (float) dy);
			speed = direction.len();
			direction.nor();
		} else {
			speed = 0;
		}
		return this;
	}

	@Override
	public ModelObject accelerate(float deltaSpeed) {
		speed += deltaSpeed;
		return this;
	}

	@Override
	public ModelObject accelerateTo(float newSpeed) {
		// TODO: should we prevent negative speed?
		speed = newSpeed;
		return this;
	}

	@Override
	public float width() {
		return size.x;
	}

	@Override
	public float height() {
		return size.y;
	}

	@Override
	public float speed() {
		return speed;
	}

	@Override
	public ModelObject moveTo(double x, double y) {
		position.set((float) x, (float) y);
		return this;
	}

	@Override
	public ModelObject moveTo(Vector2 dest) {
		position.set(dest);
		return this;
	}

	public Vector2 getPosition() {
		Map<String, String> map = new HashMap<>();

		for (Map.Entry<String, String> e : map.entrySet()) {
			System.out.println(e.getKey() + ":" + e.getValue());
		}
		return position;
	}

	@Override
	public ModelObject move(double dx, double dy) {
		position.add((float) dx, (float) dy);
		return this;
	}

	@Override
	public Vector2 velocity() {
		return new Vector2(direction).scl(speed);
	}

	@Override
	public Vector2 velocity(Vector2 dest) {
		return dest.set(direction).scl(speed);
	}

	@Override
	public Vector2 position() {
		return new Vector2(position);
	}

	@Override
	public ModelObject move(Vector2 dxy) {
		position.add(dxy);
		return this;
	}

	@Override
	public ModelObject accelerateTo(Vector2 newVelocity) {
		return accelerateTo(newVelocity.x, newVelocity.y);
	}

	@Override
	public float energy() {
		return energy;
	}

	@Override
	public boolean eat(ModelObject other, float deltaTime, World world) {
		if (energy < maxEnergy) {
			float eatAmount = Math.min(deltaTime * maxBite, other.energy());
			if (eatAmount > 0) {
				speed = 0;
				other.damage(eatAmount);
				energy = Math.min(energy + 2.7f * eatAmount, maxEnergy);
				// System.out.printf("yum %.2f→%.2f/%.2f! ", eatAmount, energy, maxEnergy);
				return true;
			}
		}
		return false;
	}

	@Override
	public float scale() {
		return energy / maxEnergy;
	}

	@Override
	public ModelObject damage(float amount) {
		energy = Math.max(0, energy - amount);
		return this;
	}

	@Override
	public ModelObject size(float newSize) {
		return size(newSize,newSize);
	}

	@Override
	public ModelObject size(float w, float h) {
		size.x = (float) w;
		size.y = (float) h;
		if(maxEnergy == 0) {
			maxEnergy = size.x * size.y;
			energy = maxEnergy;
			maxBite = maxEnergy/10;
		}
		return this;
	}

	public Vector2 size() {
		return new Vector2(size);
	}
}
