package inf112.variant2.model;

import java.util.ArrayList;
import java.util.List;

public class AppleImpl extends AbstractModelImpl {
	public static final String NAME = "apple";
	public static final String TEXTURE = "apple.png";

	public AppleImpl() {
		super(NAME);
		//Integer i1 = Integer.valueOf(50);
		//Integer i2 = new Integer(50);
	}

	@Override
	public ModelObject act(float deltaTime, World world) {
		return this;
	}

	@Override
	public String skin() {
		if(energy > .9f * maxEnergy)
			return textures.get(0);
		else if(energy > .75f * maxEnergy)
			return textures.get(1);
		else if(energy > .25f * maxEnergy)
			return textures.get(2);
		else
			return textures.get(3);
	}

	@Override
	public float scale() {
		if(skin().equals("apple-4.png"))
			return super.scale();
		else
			return 1;
	}
}
