package inf112.variant2.model;

import java.util.List;

public interface World {
	List<ModelObject> objects();
	
	void remove(ModelObject obj);
	
}
