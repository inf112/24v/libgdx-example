package inf112.variant2.model;

import com.badlogic.gdx.math.Vector2;

import inf112.variant2.view.Viewable;

/**
 * @author anya
 *
 */
public interface ModelObject extends Viewable {
	public static final float MAX_SPEED = 1000;
	public static final float MIN_SPEED = 100;

	/**
	 * Perform one time step
	 * 
	 * @param deltaTime Elapsed time since last update (in seconds)
	 * @return TODO
	 */
	ModelObject act(float deltaTime, World world);

	String name();

	/**
	 * How this ModelObject should be drawn
	 */
	String skin();

	/**
	 * @return X coordinate
	 */
	float x();

	/**
	 * @return X coordinate
	 */
	float y();

	/**
	 * @return Current direction, as a normalised vector
	 */
	Vector2 direction();

	/**
	 * @return Width of object
	 */
	float width();

	/**
	 * @return Height of object
	 */
	float height();

	/**
	 * @return Speed of object (length of velocity vector) in pixels/second
	 */
	float speed();

	/**
	 * @return A copy of the velocity vector
	 */
	Vector2 velocity();

	/**
	 * Write velocity to a vector and return
	 * 
	 * @param dest Destination vector
	 * @return dest
	 */
	Vector2 velocity(Vector2 dest);

	/**
	 * 
	 * @return "walking", "jumping", "standing"
	 */
	String state();

	/**
	 * Set position
	 * 
	 * @param x new X coordinate
	 * @param y new Y coordinate
	 * @return this
	 */
	ModelObject moveTo(double x, double y);

	/**
	 * Set position
	 * 
	 * @param newPos new (x,y) coordinates
	 * @return this
	 */
	ModelObject moveTo(Vector2 newPos);

	/**
	 * Change position
	 * 
	 * @param dx value to add to X
	 * @param dy value to add to Y
	 * @return this
	 */
	ModelObject move(double dx, double dy);

	/**
	 * Set velocity
	 * 
	 * @param dx new horizontal speed
	 * @param dy new vertical speed
	 * @return this
	 */
	ModelObject accelerateTo(double dx, double dy);

	/**
	 * Change velocity.
	 * 
	 * @param ddx horizontal change
	 * @param ddy vertical change
	 * @return this
	 */
	ModelObject accelerate(double ddx, double ddy);

	/**
	 * Set direction.
	 * 
	 * @param direction
	 * @return
	 */
	ModelObject turnTo(Vector2 direction);

	/**
	 * @return A copy of the position vector
	 */
	Vector2 position();

	/**
	 * Relative move
	 * 
	 * @param dxy A vector (dx, dy)
	 * @return this
	 */
	ModelObject move(Vector2 dxy);

	/**
	 * Set direction and speed.
	 * 
	 * @param newVelocity
	 * @return this
	 */
	ModelObject accelerateTo(Vector2 newVelocity);

	/**
	 * Set speed
	 * 
	 * @param speed
	 * @return this
	 */
	ModelObject accelerateTo(float speed);

	/**
	 * Add to current speed
	 * 
	 * @param deltaSpeed
	 * @return this
	 */
	ModelObject accelerate(float deltaSpeed);

	/**
	 * @return Current energy level
	 */
	float energy();

	/**
	 * Try to eat an object.
	 * 
	 * TODO: maybe move this to a separate Creature interface? Not all thing eat...
	 * 
	 * 
	 * @param other     The object we should try to eat.
	 * @param deltaTime Delta time of current time step
	 * @param world     The world
	 * @return true if we actually ate anything
	 */
	boolean eat(ModelObject other, float deltaTime, World world);

	/**
	 * Apply damage / reduce energy.
	 * 
	 * @param amount Amount of energy to subtract
	 * @return this
	 */
	ModelObject damage(float amount);

	/**
	 * @return Current direction, as an angle (in degrees)
	 */
	float angle();

	ModelObject accelerate(Vector2 velocity);

	Vector2 size();
	
	ModelObject size(float newSize);

	ModelObject size(float w, float h);
}