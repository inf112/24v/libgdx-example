package inf112.variant2.model;

import java.util.List;

import inf112.variant2.model.behaviour.BounceBehaviour;

class GatorImpl extends AbstractModelImpl {
	public static final String NAME = "obligator";
	public static final String TEXTURE = "obligator.png";

	public GatorImpl() {
		this(NAME, List.of(TEXTURE), List.of(new BounceBehaviour()));
	}
	public GatorImpl(String name, List<String> textures, List<Behaviour> behaviours) {
		super(name, textures, behaviours);
	}

	@Override
	public ModelObject act(float deltaTime, World world) {
		super.act(deltaTime, world);
		return this;
	}



}
