package inf112.variant2.model;

public interface Behaviour {

	boolean act(ModelObject obj, float deltaTime, World world);

}
