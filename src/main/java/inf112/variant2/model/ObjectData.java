package inf112.variant2.model;

import java.util.ArrayList;
import java.util.List;

public class ObjectData {
	public String kind;
	public String implementBy;
	public float baseWidth, baseHeight;
	public float x, y;
	public float scale = 1.0f;
	public float maxEnergy;
	public float maxBite;
	public String texture;
	public List<String> textures = new ArrayList<>();
	public List<String> behaviours = new ArrayList<>();

	public float baseHeight() {
		return baseHeight <= 0 ? baseWidth : baseHeight;
	}

	public float baseWidth() {
		return baseWidth <= 0 ? baseHeight : baseWidth;
	}

	public List<String> textures() {
		if (textures.isEmpty())
			textures.add(texture);
		return textures;
	}
}
