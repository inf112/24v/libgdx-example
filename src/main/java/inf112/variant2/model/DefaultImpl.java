package inf112.variant2.model;

import java.util.List;

public class DefaultImpl extends AbstractModelImpl {
	public static final String NAME = "obligator";
	public static final String TEXTURE = "obligator.png";

	public DefaultImpl() {
		this("", List.of(), List.of());
	}

	public DefaultImpl(String name, List<String> textures, List<Behaviour> behaviours) {
		super(name, textures, behaviours);
	}

	@Override
	public ModelObject act(float deltaTime, World world) {
		super.act(deltaTime, world);
		return this;
	}

}
