package inf112.variant2.model.behaviour;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import inf112.flocking.FlockingApp;
import inf112.variant2.model.Behaviour;
import inf112.variant2.model.ModelObject;
import inf112.variant2.model.World;

/**
 * Make the object participate in a flock.
 * 
 * Works by (slowly) changing direction based on flocking rules: separation
 * (steer away if too close), alignment (steer towards the same heading as other
 * flock members), cohesion (steer toward the other flock members).
 * 
 * See https://en.wikipedia.org/wiki/Flocking
 * 
 * 
 * @author anya
 *
 */
public class FlockingBehaviour implements Behaviour {

	private static final float MAX_SPEED = 1000;
	private static final float MIN_SPEED = 100;
	private static final float BORDER = 0.1f;

	@Override
	public boolean act(ModelObject obj, float deltaTime, World world) {
		float size2 = obj.width() * obj.height();
		float sightLimit = 30 * size2, avoidThreshold = 5 * size2;
		float W = 1920, H = 1080;
		var direction = obj.velocity();
		var speed = direction.len();
		direction.nor();
		var v0 = obj.position();// .sub(obj.width()/2, obj.height()/2);
		var v1 = obj.position();// .add(obj.width()/2, obj.height()/2);
//		var v0 = localToStageCoordinates(new Vector2(getX(Align.bottomLeft)+dx, getY(Align.bottomLeft)+dy));
//		var v1 = localToStageCoordinates(new Vector2(getX(Align.topRight)+dx, getY(Align.topRight)+dy));
		var d = new Vector2(direction).scl(deltaTime * MAX_SPEED);
		// d.scl(0);
		
		// *** FIRST: steer away from walls
		var tmp = new Vector2(direction);
		var borderX = W * BORDER;
		var borderY = H * BORDER;
		v0.sub(borderX * .5f, borderY * 2);
		v0.x = Math.max(0, -v0.x) / borderX;
		v0.y = Math.max(0, -v0.y) / (borderY * 2);

		v1.sub(W - borderX * .5f, H - borderY * .1f);
		v1.x = Math.max(0, v1.x) / borderX;
		v1.y = Math.max(0, v1.y) / borderY;
		var wallDist = v0.x + v0.y + v1.x + v1.y;
		// wallDist = 2f - 5*wallDist;
		// wallDist *= wallDist;
		// if (this == FlockingApp.gator)
		// System.out.println(v0 + " " + v1 + " " + wallDist);
		if (wallDist > 0)
			speed = Interpolation.linear.apply(speed, MIN_SPEED, .05f * wallDist);
		else
			speed = Interpolation.linear.apply(speed, MAX_SPEED, .01f);
		// speed = (8*speed + 2 * wallDist*speed) / 10f;
		direction.add(v0.scl(v0).scl(.5f)).sub(v1.scl(v1).scl(.5f)).nor();
		d.set(direction).scl(deltaTime * speed);
		obj.move(d.x, d.y);
		v0.set(obj.position());
		v1.set(v0);
		d.set(direction);
		var avgSpeed = speed;
		int count = 1;
		for (ModelObject other : world.objects()) {
			if (other == this || other.getClass() != obj.getClass())
				continue;

			tmp.set(other.position());
			var distance = v0.dst2(tmp);
			if (distance > sightLimit)
				continue;
			// accumulate average position...
			v1.add(tmp);
			// and velocity
			d.add(other.velocity().nor().scl((sightLimit - distance) / sightLimit));
			avgSpeed += other.speed();
			count++;

//			var directionAway = new Vector2(v0);
//			directionAway.sub(tmp);
//			directionAway.nor();
//			
			// **** AVOID nearby flock mates
			var directionAway = new Vector2(v0).sub(tmp).nor();
			if (distance > 0) {
				// directionAway.rotateDeg(180);
			}
			// if (a == HelloWorld.gator)
			// System.out.println("dist=" + distance + ", size=" + size2 + ", angle=" +
			// direction + ", v=" + velocity + ", s=" + speed);

			if (distance < avoidThreshold) {
				var factor = (avoidThreshold - Math.max(0f, distance)) / avoidThreshold;
				direction.lerp(directionAway, factor * factor / 3f).nor();
			}

//			if(distance < 5*5*size) {
//				direction.lerp(new Vector2(other.velocity).nor(), .2f);
//			}

			// Vector2 v = this.
		}
		direction.lerp(new Vector2(Math.signum(direction.x), 0), .005f);
		if (count > 1) {
			v1.scl(1f / count);
			var directionToCenter = new Vector2(v1).sub(v0).nor();
			d.nor();
			// **** Steer towards average position
			direction.lerp(directionToCenter, 0.05f).nor();
			// **** Steer towards average heading
			direction.lerp(d, .15f);
			if (FlockingApp.target.x != 0f || FlockingApp.target.y != 0f) {
				tmp.set(FlockingApp.target).sub(v0);
				var maxLen2 = W * H;
				var distFactor = Math.max(0, (maxLen2 - tmp.len2()) / maxLen2);
				var directionToTarget = tmp.nor();
				direction.lerp(directionToTarget, 0.5f * distFactor).nor();
			}

		}
		// System.out.print(count + " ");
		avgSpeed /= count;
		speed = (60 * speed + 40 * avgSpeed) / 100f;
		direction.scl(speed);
		obj.accelerateTo(direction);
		// setRotation((direction.angleDeg() + 180) % 360f);
		return true;
	}

}
