package inf112.variant2.model.behaviour;

import inf112.variant2.model.Behaviour;
import inf112.variant2.model.ModelObject;
import inf112.variant2.model.World;

/**
 * Basic move behaviour: update position based on velocity, consume a little bit
 * of energy.
 * 
 * @author anya
 *
 */
public class MoveBehaviour implements Behaviour {

	@Override
	public boolean act(ModelObject obj, float deltaTime, World world) {
		var move = obj.direction().scl(obj.speed() * deltaTime);
		obj.damage(10 * deltaTime);
		//obj.damage(obj.speed() * deltaTime);
		obj.move(move);

		return true;
	}

}
