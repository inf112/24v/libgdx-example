package inf112.variant2.model.behaviour;

import java.util.function.Predicate;

import inf112.variant2.model.Behaviour;
import inf112.variant2.model.ModelObject;
import inf112.variant2.model.World;

/**
 * Returns true if the object can see food nearby.
 * 
 * @author anya
 *
 */
public class CanSeeFoodPredicate implements Behaviour {

	private Predicate<ModelObject> isFood;
	
	public CanSeeFoodPredicate(Predicate<ModelObject> isFood) {
		this.isFood = isFood;
	}
	
	
	@Override
	public boolean act(ModelObject obj, float deltaTime, World world) {
		float size2 = obj.width()*obj.height();
		float sightLimit = 30*size2;

		for (ModelObject other : world.objects()) {
			if (other == this)
				continue;
			
			var otherPos = other.position();
			var distance = obj.position().dst2(otherPos);
			if (distance > sightLimit)
				continue;
			
			if(isFood.test(other)) {
				return true;
			}
		}
		
		return false;
	}

}
