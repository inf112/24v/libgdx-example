package inf112.variant2.model.behaviour;

import java.util.function.Predicate;

import com.badlogic.gdx.math.Vector2;

import inf112.variant2.model.Behaviour;
import inf112.variant2.model.ModelObject;
import inf112.variant2.model.World;

/**
 * Steer / move towards closes edible object.
 * 
 * @author anya
 *
 */
public class GotoFoodBehaviour implements Behaviour {

	private Predicate<ModelObject> isFood;

	public GotoFoodBehaviour(Predicate<ModelObject> isFood) {
		this.isFood = isFood;
	}

	@Override
	public boolean act(ModelObject obj, float deltaTime, World world) {
		float size = (float) Math.sqrt(obj.width() * obj.height());
		float sightLimit = 30 * size;
		ModelObject nearbyFood = null;
		float closest = Float.MAX_VALUE;
		for (ModelObject other : world.objects()) {
			if (other == this)
				continue;

			var otherPos = other.position();
			var distance = obj.position().dst(otherPos);
			if (distance > sightLimit)
				continue;

			if (isFood.test(other) && distance < closest) {
				closest = distance;
				nearbyFood = other;
			}
		}
		if (nearbyFood != null) {
			Vector2 towardsFood = nearbyFood.position().sub(obj.position()).nor();
			Vector2 direction = obj.direction();
			float speed = obj.speed();
			if (speed*3 < closest)
				speed *= 1.1f;
			else
				speed /= 1.1f;
			speed = Math.min(Math.max(speed, ModelObject.MIN_SPEED), ModelObject.MAX_SPEED);
			direction.lerp(towardsFood, .1f);
			obj.turnTo(direction);
			obj.accelerateTo(speed);
			//System.out.print("G");
			return true;
		}

		return false;
	}

}
