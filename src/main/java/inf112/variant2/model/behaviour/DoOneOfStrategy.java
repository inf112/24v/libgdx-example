package inf112.variant2.model.behaviour;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import inf112.variant2.model.Behaviour;
import inf112.variant2.model.ModelObject;
import inf112.variant2.model.World;

/**
 * Pick a random behaviour that returns true.
 * 
 * That is, try the given behaviours in random order, until one
 * of them returns true.
 * 
 * @author anya
 *
 */
public class DoOneOfStrategy implements Behaviour {
	private Random random = new Random();
	private List<Behaviour> behaviours;
	
	public DoOneOfStrategy(Behaviour...behaviours) {
		this.behaviours = List.of(behaviours);
	}
	
	@Override
	public boolean act(ModelObject obj, float deltaTime, World world) {
		var bs = new ArrayList<>(behaviours);
		Collections.shuffle(bs);
		
		for(var b : bs) {
			if(b.act(obj, deltaTime, world))
				return true;
		}
		return false;
	}

}
