package inf112.variant2.model.behaviour;

import java.util.function.Predicate;

import inf112.variant2.model.Behaviour;
import inf112.variant2.model.ModelObject;
import inf112.variant2.model.World;

// Abstract Syntax Tree node for if-then-else
public class IfThenElse implements Behaviour {

	
	private Behaviour cond;
	private Behaviour thenClause;
	private Behaviour elseClause;
	
	public IfThenElse(Behaviour cond, Behaviour thenClause, Behaviour elseClause) {
		this.cond = cond;
		this.thenClause = thenClause;
		this.elseClause = elseClause;
	}

	@Override
	public boolean act(ModelObject obj, float deltaTime, World world) {
		if(cond.act(obj, deltaTime, world))
			return thenClause.act(obj, deltaTime, world);
		else if(elseClause != null)
			return elseClause.act(obj, deltaTime, world);
		else
			return false;
	}

}
