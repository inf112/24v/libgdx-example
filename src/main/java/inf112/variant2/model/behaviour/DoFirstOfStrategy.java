package inf112.variant2.model.behaviour;

import java.util.List;

import inf112.variant2.model.Behaviour;
import inf112.variant2.model.ModelObject;
import inf112.variant2.model.World;

/**
 * Pick the first of the given behaviours that return true.
 * 
 * @author anya
 *
 */
public class DoFirstOfStrategy implements Behaviour {

	private List<Behaviour> behaviours;
	
	public DoFirstOfStrategy(Behaviour...behaviours) {
		this.behaviours = List.of(behaviours);
	}
	
	@Override
	public boolean act(ModelObject obj, float deltaTime, World world) {
		for(var b : behaviours) {
			if(b.act(obj, deltaTime, world))
				return true;
		}
		return false;
	}

}
