package inf112.variant2.model.behaviour;

import com.badlogic.gdx.math.Vector2;

import inf112.util.Calculations;
import inf112.variant2.model.Behaviour;
import inf112.variant2.model.ModelObject;
import inf112.variant2.model.World;

/**
 * Will cause the object to "bounce off" the screen boundaries if its next move
 * will put it outside the screen.
 * 
 * @author anya
 *
 */
public class BounceBehaviour implements Behaviour {

	@Override
	public boolean act(ModelObject obj, float deltaTime, World world) {
		var direction = obj.direction();
		var speed = Math.min(Math.max(obj.speed(), ModelObject.MIN_SPEED), ModelObject.MAX_SPEED);
speed =obj.speed();
		var move = new Vector2(direction).scl(speed * deltaTime);
		var outside = Calculations.checkBorder(obj.position(), move, new Vector2(1920, 1080));

		if (outside.x != 0f)
			direction.scl(-.97f, 1);
		if (outside.y != 0f)
			direction.scl(1, -.5f);
		obj.turnTo(direction);
		move.set(direction).scl(speed * deltaTime);
		obj.move(move);

		return true;
	}

}
