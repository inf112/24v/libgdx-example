package inf112.variant2.model.behaviour;

import java.util.function.Predicate;

import inf112.variant2.model.Behaviour;
import inf112.variant2.model.ModelObject;
import inf112.variant2.model.World;

/**
 * Calls the object's eat() method if food is within reach.
 * 
 * @author anya
 *
 */
public class EatFoodBehaviour implements Behaviour {

	private Predicate<ModelObject> isFood;
	
	public EatFoodBehaviour(Predicate<ModelObject> isFood) {
		this.isFood = isFood;
	}
	
	
	@Override
	public boolean act(ModelObject obj, float deltaTime, World world) {
		float size = (float) Math.sqrt(obj.width()*obj.height());
		float eatLimit = size/2;

		for (ModelObject other : world.objects()) {
			if (other == this)
				continue;
			
			var otherPos = other.position();
			var distance = obj.position().dst(otherPos);
			if (distance > eatLimit)
				continue;
			
			if(isFood.test(other)) {
				//System.out.print("E");
				return obj.eat(other, deltaTime, world);
			}
		}
		
		return false;
	}

}
