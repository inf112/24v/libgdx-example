package inf112.variant2.app;

import static spark.Spark.*;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonValue.ValueType;
import com.badlogic.gdx.utils.JsonWriter.OutputType;

/**
 * Example of using a web browser to display data from a running game.
 * 
 * Start the game by running this class; it'll open a tiny webserver at
 * http://localhost:4567/
 * 
 * This tiny example isn't particularly useful, it just returns information 
 * about game objects as JSON data. Could be used for debugging, but it's
 * probably more convenient to just use an *actual* debugger for this.
 * 
 * But, it's very easy to display data or create user interfaces in the web
 * browser, and it's probably easier to get a good result than with Swing
 * or JavaFX. VSCode works like this, for example – it's just a HTML5/JavaScript
 * application running in Chrome; if you use it for Java, it'll talk to Eclipse JDT
 * over a network port to get the Java IDE functionality.
 * 
 *
 */
public class WebApp {
	public static void main(String[] args) {
		// where the server should listen (localhost, in this case)
		ipAddress("127.0.0.1");
		// if we want to serve static files, put them in src/main/resources/public
		staticFiles.location("/public");
		var json = new Json();
		json.setOutputType(OutputType.json);
		json.setUsePrototypes(false);
		
		// WARNING: this web application will run in a separate thread –
		// Spark will probably start ten threads or so to handle incoming requests.
		// This means that the handler code below might be running *at the same time*
		// as the game code. For example, while the game might be calling the player's
		// move() method while we're in the middle of converting the player data to
		// JSON. This will lead to inconsistencies. It's not so bad as long as we're
		// only reading data and showing it in a web browser for debug purposes, but
		// if we were making some form of multiplayer game or were using the browser
		// as another way for a player to interact with the game, we'd have to use some
		// form of synchronization to avoid issues. Learn more about this in INF214.

		// EXAMPLE: serve ModelObjects by encoding them as JSON
		get("/player", (req, res) -> { // dump player data
			res.type("application/json");
			return json.toJson(Controller.instance.player);
		});
		// EXAMPLE: serve any ModelObject in the game world. We should
		// probably give each object a unique ID, instead of just using
		// its index in the list of objects.
		get("/objects/:id", (req, res) -> { // dump object by number
			// very rudimentary parameter decoding (should check for errors)
			int id = Integer.parseInt(req.params("id"));
			res.type("application/json");
			return json.toJson(Controller.instance.models.get(id));
		});
		// EXAMPLE: serve all the ModelObjects, using a simple pagination
		// scheme, so we avoid dumping thousands of objects in one request.
		// We could use this to draw the entire game world in the web browser.
		get("/objects/", (req, res) -> { // dump object by number
			// simple 
			int page = Integer.parseInt(req.queryParamOrDefault("page", "0"));
			int limit = Integer.parseInt(req.queryParamOrDefault("limit", "100"));
			int start = page * limit;
			int end = Math.min(start + limit, Controller.instance.models.size());
			var result = "[]";
			if (start < Controller.instance.models.size()) {
				result = json.toJson(Controller.instance.models.subList(start, end));
			}
			if (end < Controller.instance.models.size()) // there is a next page
				res.header("Link", String.format("<%s?page=%d&limit=%d>; rel=\"next\"", "", page + 1, limit));
			if (page > 0) // there is a previous page
				res.header("Link", String.format("<%s?page=%d&limit=%d>; rel=\"prev\"", "", page - 1, limit));

			res.type("application/json");
			return result;
		});
		// EXAMPLE: change player object with a PUT request
		// (this is even more unsafe than just reading data
		put("/player/move", (req, res) -> { // move the player
			// very rudimentary parameter decoding (should check for errors)
			float dx = Float.parseFloat(req.queryParamOrDefault("dx", "0"));
			float dy = Float.parseFloat(req.queryParamOrDefault("dy", "0"));
			Controller.instance.player.move(dx, dy);
			// return the new position as a json object
			var result = new JsonValue(ValueType.object);
			result.addChild("x", new JsonValue(Controller.instance.player.x()));
			result.addChild("y", new JsonValue(Controller.instance.player.y()));

			res.type("application/json");
			return result.toJson(OutputType.json);
			// return halt(404, errorPage("404 Not Found"));
		});

		// Start the game. Note that Spark will start its own little server thread pool
		// once we start adding routes (get/put/etc above); any web requests will be 
		// handled by those threads. We could just return here if we wanted to, and
		// the program would keep running and processing requests. Instead, we start
		// a LibGDX application, which will do it's startup stuff and then run its
		// endless game loop – while Spark continues to listen for and deal with web requests. 
		MVCDemo.main(args);
		System.exit(0); // make sure we shutdown all threads at the end
	}

	public static String errorPage(String error) {
		return String.format("<html><head><title>%s</title></head><body><h1>%s</h1></body></html>", error, error);
	}

	public static String htmlDoc(String title, String body) {
		String style = "<link rel=\"stylesheet\" href=\"/style.css\">\n";
		String icon = "<link rel=\"icon\" type=\"image/svg+xml\" href=\"/favicon.svg\">\n";

		return String.format(
				"<!DOCTYPE html>\n<html>\n<head>\n<title>%s</title>\n%s%s</head>\n<body>\n%s</body>\n</html>\n", title,
				icon, style, body);
	}
}
