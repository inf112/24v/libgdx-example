package inf112.variant2.app;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter.OutputType;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import inf112.codeexamples.SingletonExample;
import inf112.variant2.model.AppleImpl;
import inf112.variant2.model.DefaultImpl;
import inf112.variant2.model.ModelFactory;
import inf112.variant2.model.ModelObject;
import inf112.variant2.model.World;
import inf112.variant2.view.View;

public class Controller implements ApplicationListener {
	public static Controller instance;
	public static final int WORLD_WIDTH = 1920, WORLD_HEIGHT = 1080;
	private static final int SENSITIVITY = 100;
	private Sound bellSound;
	private View view;
	private Random random = new Random();
	private Viewport viewport;
	private Vector2 target = null;

	// probably better to move this to the model package and call it World or
	// something
	List<ModelObject> models = new ArrayList<>();
	ModelObject player;
	boolean paused = false;
	private int frame;

	private Logger logger;

	@Override
	public void create() {
		// Called at startup

		var json = new Json();
		json.setOutputType(OutputType.json);

		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		ModelFactory.autoInitialize();
		ModelFactory.initializeFromJson();
		Gdx.app.log("MainController", "hello");
		viewport = new FitViewport(1920, 1080);
		bellSound = Gdx.audio.newSound(Gdx.files.internal("blipp.ogg"));
		view = new View(viewport);
		view.loadTextures(ModelFactory::kinds);

		var listener = new InputAdapter() {
			@Override
			public boolean touchDown(int screenX, int screenY, int pointer, int button) {
				if (button == Buttons.LEFT) {
					target = new Vector2(screenX, screenY);
					viewport.unproject(target);

					return true;
				} else {
					return false;
				}
			}

			@Override
			public boolean touchDragged(int screenX, int screenY, int button) {
				if (target != null && button == Buttons.LEFT) {
					target.set(screenX, screenY);
					viewport.unproject(target);
					System.out.println(target);
					return true;
				}
				return false;
			}

			@Override
			public boolean touchUp(int screenX, int screenY, int pointer, int button) {
				target = null;
				return true;
			}

			@Override
			public boolean keyDown(int keyCode) {
				if (keyCode == Keys.LEFT || keyCode == Keys.A) {
					System.out.println("left");
					player.accelerate(-10, 0);
					return true;
				} else if (keyCode == Keys.RIGHT || keyCode == Keys.D) {
					System.out.println("right");
					player.accelerate(10, 0);
					return true;
				} else if (keyCode == Keys.ESCAPE) {
					Gdx.app.exit();
				} else if (keyCode == Keys.Z) {
					try {
						json.toJson(models, new PrintWriter("/tmp/log.json"));
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// System.out.println(json.toJson(models));
				}
				return false;

			}
		};
		var multiplexer = new InputMultiplexer(listener);
		Gdx.input.setInputProcessor(multiplexer);

		loadScene("level1");

		Gdx.graphics.setForegroundFPS(60);

		// eksempel: lagre og lese inn eple:

		ModelObject apple = ModelFactory.create("apple");
		apple.size(200);
		apple.moveTo(100, 100);
		var eple = json.toJson(apple); // eple i json

		// json.setTypeName("class");
		var jsonApple = json.toJson(List.of(apple));
		System.out.println(jsonApple);
		List<ModelObject> fromJson = json.fromJson(List.class, jsonApple);
		System.out.println("lest inn: " + fromJson);
		models.add(fromJson.get(0));

		// SingletonExample.instance.getState();
		// SingletonExample.getInstance().getState();
		Controller.instance = this;

	}

	private Vector2 randomPosition() {
		return new Vector2((.25f + random.nextFloat() / 2) * WORLD_WIDTH,
				(.25f + random.nextFloat() / 2) * WORLD_HEIGHT);
	}

	private float randomSpeed() {
		return 500 * (random.nextFloat() - .5f); // random.nextGaussian()*100
	}

	private void loadScene(String scene) {
		var objectKinds = ModelFactory.kinds();

		player = ModelFactory.create("obligator");
		player.size(200);
		player.moveTo(randomPosition());

		// since we create stuff based on names, we could just as easily load the
		// level from disk
		if (scene.equals("level1")) {
			for (int i = 0; i < 1500; i++) {
				if (true) { // random.nextInt(10) < 8) {
					var model = ModelFactory.create("pipp");
					model.size(random.nextFloat(20, 21));
					model.moveTo(randomPosition().add(-100, 1500)).accelerate(0, -500);
					models.add(model);
				} else {
					var kind = objectKinds.get(random.nextInt(objectKinds.size()));
					var model = ModelFactory.create(kind);
					model.size(random.nextFloat(20, 21));
					model.moveTo(randomPosition()).accelerateTo(randomSpeed(), randomSpeed());
					models.add(model);
				}
			}
		}

		models.add(player);
	}

	@Override
	public void dispose() {
		// Called at shutdown

		// Graphics and sound resources aren't managed by Java's garbage collector, so
		// they must generally be disposed of manually when no longer needed. But,
		// any remaining resources are typically cleaned up automatically when the
		// application exits, so these aren't strictly necessary here.
		// (We might need to do something like this when loading a new game level in
		// a large game, for instance, or if the user switches to another application
		// temporarily (e.g., incoming phone call on a phone, or something).

		bellSound.dispose();
	}

	@Override
	public void render() {
		if (paused)
			return;

		float delta = Gdx.graphics.getDeltaTime();

		processInputState(delta);
		act(delta);
		view.drawFrame(models);
	}

	private void processInputState(float deltaTime) {
		var amount = SENSITIVITY * deltaTime;

		// for these keys, we want something to happen *while they are pressed*,
		// so we use Gdx.input.isKeyPressed() rather than a listener.
		//
		// Some keys (like left/right and up/down) conflict with each other, so
		// it would perhaps be better to track KeyDown and KeyUp, and pick
		// the last pressed one in case of a conflict.
		if (Gdx.input.isKeyPressed(Keys.A) || Gdx.input.isKeyPressed(Keys.LEFT))
			player.accelerate(-amount, 0);
		else if (Gdx.input.isKeyPressed(Keys.D) || Gdx.input.isKeyPressed(Keys.RIGHT))
			player.accelerate(amount, 0);
		else if (Gdx.input.isKeyPressed(Keys.W) || Gdx.input.isKeyPressed(Keys.UP))
			player.accelerate(0, amount);
		else if (Gdx.input.isKeyPressed(Keys.S) || Gdx.input.isKeyPressed(Keys.DOWN))
			player.accelerate(-0, -amount);

	}

	public void act(float delta) {
		if (paused)
			return;
		// probably better to have a World (or Stage) model rather
		// than working with a list in the controller
		frame++;
		World world = new World() { // TODO: move to models package
			@Override
			public List<ModelObject> objects() {
				return Collections.unmodifiableList(new ArrayList<>(models));
			}

			@Override
			public void remove(ModelObject obj) {
				models.remove(obj);
			}
		};
		var nApples = 0;
		for (var o : world.objects()) {
			o.act(delta, world);
		}
		for (var o : world.objects()) {
			if (o.energy() < 1) {
				// System.out.println("Removed " + o);
				world.remove(o);
			} else if (o.name().equals("apple")) {
				nApples++;
			}
		}
		if (frame % 2 == -1) {
			var model = ModelFactory.create("pipp");
			model.size(random.nextFloat(20, 21));
			var offset = 50;
			model.moveTo(860, 50).accelerateTo(random.nextGaussian(), 1000);
			models.add(model);
//			model = ModelFactory.create("pipp", random.nextDouble(20, 21));
//			model.moveTo(800-offset,900).accelerateTo(1*offset, 0);
//			models.add(model);		
		}
		if (random.nextInt(10) > 4 && target != null) {
			var model = ModelFactory.create("apple");
			model.size(random.nextFloat(20, 50));
			var pos = new Vector2(target);
			pos.add(random.nextFloat() * .1f * WORLD_WIDTH - .05f * WORLD_WIDTH, //
					random.nextFloat() * .1f * WORLD_HEIGHT - .05f * WORLD_HEIGHT);
			model.moveTo(pos);
			models.add(0, model);
		} else if (random.nextInt(10) > 8 && nApples < 100) {
			var model = ModelFactory.create("apple");
			model.size(random.nextFloat(20, 50));
			var pos = new Vector2(random.nextFloat() * WORLD_WIDTH, //
					random.nextFloat() * .25f * WORLD_HEIGHT);
			model.moveTo(pos);
			models.add(0, model);
		}

// System.out.println();
		// Alternative approach: every change create a new immutable object,
		// and we get a new world (model list) for every turn:
		//
		// models = models.stream().map(obj -> obj.act(delta, world)).toList();
	}

	@Override
	public void resize(int width, int height) {
		// Called whenever the window is resized (including with its original site at
		// startup)

		viewport.update(width, height, true);

		System.out.printf("(%d,%d)=(%d,%d)  (%.2f,%.2f)%n", width, height, viewport.getScreenWidth(),
				viewport.getScreenHeight(), viewport.getWorldWidth(), viewport.getWorldHeight());
	}

	@Override
	public void pause() {
		paused = true;
		System.out.println("PAUSE!");
	}

	@Override
	public void resume() {
		paused = false;
		System.out.println("RESUME!");
	}
}
