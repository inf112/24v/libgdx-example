package inf112.variant2.app;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

public class MVCDemo {
    public static void main(String[] args) {
        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setTitle("MVC Demo");
        cfg.setWindowedMode(1920, 1080);

        new Lwjgl3Application(new Controller(), cfg);
        System.exit(0);
    }
}