package inf112.skeleton.app;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.opentest4j.AssertionFailedError;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.MatcherAssert.assertThat;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.particles.influencers.ColorInfluencer.Random;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import inf112.codeexamples.Möckitö;
import inf112.variant1.app.Fish;
import inf112.variant1.app.Gator;
import inf112.variant2.model.ModelObject;

/**
 * Unit tests (example).
 * 
 * (Run using `mvn test`)
 */
public class MockitoTest {
	/**
	 * Static method run before everything else
	 */
	@BeforeAll
	static void setUpBeforeAll() {
		HeadlessApplicationConfiguration config = new HeadlessApplicationConfiguration();
		ApplicationListener listener = new ApplicationAdapter() {
		};
		new HeadlessApplication(listener, config);
	}

	/**
	 * Setup method called before each of the test methods
	 */
	@BeforeEach
	void setUpBeforeEach() {
	}

	@Test
	@SuppressWarnings("unchecked")
	void mockTest() {
		List<String> realList = new ArrayList<>();
		List<String> mockedList = mock(List.class);

		when(mockedList.add("mango")).thenReturn(true); // (1)
		when(mockedList.contains("banana")).thenReturn(true); // (2)

		assertTrue(realList.add("mango"));
		assertTrue(mockedList.add("mango")); // ok, pga (1)

		// samme test, forskjellig resultat. mockedList.toString() gir "Mock for List…"
		assertEquals("[mango]", realList.toString());
		assertThrows(AssertionFailedError.class, //
				() -> assertEquals("[mango]", mockedList.toString()));

		assertEquals(1, realList.size()); // ok
		assertThrows(AssertionFailedError.class, // mocket liste gir default verdi 0
				() -> assertEquals(1, mockedList.size()));

		assertTrue(realList.contains("mango")); // ok
		assertThrows(AssertionFailedError.class, // mocket liste gir default verdi false
				() -> assertTrue(mockedList.contains("mango")));
		assertTrue(mockedList.contains("banana")); // ok, pga (2)
	}

	@Test
	void mockTestWhen() {
		@SuppressWarnings("unchecked")
		List<String> mockedList = mock(List.class);

		mockedList.add("mango"); // fungerer tilsynelatende, men gjør ingenting

		// lag "implementasjoner" for size() og contains()
		when(mockedList.size()).thenReturn(1);
		when(mockedList.contains("mango")).thenReturn(true);
		when(mockedList.contains("banana")).thenReturn(false);

		assertEquals(1, mockedList.size()); // ok, vi har programmert size() til å returnere 1
		assertTrue(mockedList.contains("mango")); // også ok
		assertFalse(mockedList.contains("banana")); // også ok siden vi sa at mockedList.contains("banana") == false
		assertFalse(mockedList.contains("apple")); // også ok siden false er default
	}

	/**
	 * Simple test case
	 */
	@Test
	void testMockGDX() {
		// funker når vi bruker HeadlessApplication
		// (Å teste dette er faktisk nyttig! Det er ikke uvanlig at en del filer
		// ikke lar seg lese, enten pga. små/store bokstaver i filnavn, eller fordi
		// man har glemt å sjekke de inn i Git.)
		FileHandle file1 = Gdx.files.internal("obligator.png");
		// vi kunne mocket den, men i praksis må vi gjerne erstatte
		// ganske mye av funksjonaliteten for at det skal funke
		FileHandle file2 = mock(FileHandle.class);
		
		// Mocket texture – kan vi bruke den til å tegne med?
		Texture texture = mock(Texture.class);
		SpriteBatch batch = mock(SpriteBatch.class);
		batch.begin();
		batch.draw(texture, 0, 0); // ja, ihvertfall hvis vi mocker SpriteBatch også
		batch.end();

		// Stage trenger Viewport, Camera og SpriteBatch; så lenge vi ikke
		// tegner ting, så funker det å mocke disse
		Viewport viewport = mock(Viewport.class);
		viewport.setCamera(mock(Camera.class));
		// evt. kan vi bare overstyre getCamera()
		when(viewport.getCamera()).thenReturn(mock(Camera.class));
		Stage stage = new Stage(viewport, batch);

		Gator gator = new Gator(texture, 0, 0);
		stage.addActor(gator);
		stage.act(); // funker helt fint, kaller gator.act()
		// draw() vil komme til å feile:
		assertThrows(RuntimeException.class, () -> stage.draw());
	}

	@Test
	void testEatingByObserving() {
		// Sjekk at en alligator finner og spiser fisk i samme vanndammen
		Gator gator = new Gator(null, 0, 0);
		Fish fish = new Fish();
		Stage stage = makeStage();

		float oldGatorHealth = gator.getHealth();
		stage.addActor(gator);
		stage.addActor(fish);
		stage.act();

		// etter act() skal gator ha spist fish, så health skal ha økt...
		assertThat(gator.getHealth(), greaterThan(oldGatorHealth));
		// ...og fisken skal være død
		assertEquals(0f, fish.getHealth());
	}

	@Test
	void testEatingByMocking() {
		// Sjekk at en alligator finner og spiser fisk i samme vanndammen
		Gator gator = new Gator(null, 0, 0);
		Fish fish = mock(Fish.class);
		Stage stage = makeStage();

		stage.addActor(gator);
		stage.addActor(fish);
		stage.act();

		// etter act() skal gator ha spist fish, og kalt kill() metoden
		verify(fish).kill();
	}

	@SuppressWarnings("unchecked")
	@Test
	void testAddActorsByMocking() {
		// verify() egner seg best når vi er ute etter å teste at
		// spesifikke metoder blir kalt. For eksempel, sjekke at
		// vi sender en melding til en ekstern komponent (f.eks. over
		// nettverket), eller skriver data til en fil eller en USB enhet.
		//
		// Som regel er hvilke metoder som blir kalt implementasjonsdetaljer,
		// og det er gjerne bedre å teste oppførselen på andre måter.
		// Se testAddActorsByObserving()

		List<Actor> actors = mock(List.class);
		var gator = mock(Gator.class);
		var fish = mock(Fish.class);

		// To forskjellige måter å legge til fish og gator i listen
		if (Math.random() < 0.5) { // for demonstrasjonformål
			actors.addAll(List.of(fish, gator));
		} else {
			actors.add(fish);
			actors.add(gator);
		}

		// Testen vil feile hvis vi valgte addAll(), selv om resultatet
		// er det samme. Her har vi antakelig *overspesifisert* hva
		// programmet skal gjøre, og det er uinteressant hvordan
		// elementene havner i listen så lenge resultatet er korrekt.
		verify(actors).add(gator);
		verify(actors).add(fish);
	}

	@Test
	void testAddActorsByObserving() {
		// I mange tilfeller er det vi ser etter når vi tester
		// for "korrekt oppførsel" egentlig:
		//
		// * gitt disse argumentene gir metoden denne returverdien
		// * hvis tilstanden er *slik* før vi kaller metoden, så skal den
		// være *sånn* etterpå
		//
		// *Hvordan* dette skjer er en implementasjonsdetalj som vi ikke trenger
		// å teste for (ellers *overspesifiserer* vi hva programmet skal gjøre).

		List<Actor> actors = new ArrayList<>();
		var gator = mock(Gator.class);
		var fish = mock(Fish.class);

		// To forskjellige måter å legge til fish og gator i listen
		if (Math.random() < 0.5) {
			actors.addAll(List.of(fish, gator));
		} else {
			actors.add(fish);
			actors.add(gator);
		}

		// Vi sjekker tilstanden etterpå, og ser at den er riktig. 
		assertTrue(actors.contains(gator));
		assertTrue(actors.contains(fish));

		// Collection har også en containsAll() som kan være nyttig her:
		var expected = List.of(gator, fish);
		assertTrue(actors.containsAll(expected));
		assertEquals(expected.size(), actors.size());

		// Over brydde vi oss ikke om rekkefølgen, men det kan vi
		// selvfølgelig gjør om det er interessant (hvis det *ikke* spiller
		// noen rolle, er det bedre å ikke sjekke rekkefølgen)
		assertEquals(List.of(fish, gator), actors);
	}

	@Test
	void testWhen() {
		List<Actor> actors = mock(List.class);
		Fish fish = new Fish();

//		
		var gator = mock(Gator.class);
		fish = mock(Fish.class);
		when(fish.getHeight()).thenReturn(5f);
		when(fish.getWidth()).thenReturn(5f);
		when(fish.getX()).thenReturn(5f);
		actors.add(fish);
		actors.add(gator);


		verify(actors).add(gator);
		verify(actors).add(fish);
	}

	/**
	 * @return A Stage usable for testing (stage.draw() won't work)
	 */
	static Stage makeStage() {
		Viewport viewport = mock(Viewport.class);
		viewport.setCamera(mock(Camera.class));
		return new Stage(viewport, mock(SpriteBatch.class));
	}

	@Test
	void testActorAct() {
		// sjekker at act() metoden blir kalt når vi kaller stage.act()
		// (Dette er egentlig del av oppførselen til libGDX, så denne testen
		// hører hjemme der, ikke i vårt prosjekt.)
		
		var stage = makeStage();
		var actor = mock(Actor.class);

		stage.addActor(actor);
		stage.act(0f);

		verify(actor).act(0f);
	}
}